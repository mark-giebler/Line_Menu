/// @file Line_Menu.cpp
///
/// Line menu UI class
/// For managing Menu driven interfaces via
/// Displays or (someday) Serial ports
///
/// Mark Giebler
/// 2022-09-20

#include "Line_Menu.h"

/**
 * Constructor for when using a Adafruit_GFX based Display object for the menu.
 *
 * @param	gfx		Pointer to the display such that a menu UI can be realized on it.
 * @param	autoNo	Auto number menu items on display.
 * @param	showDsp	Pointer to function that updates actual screen with data.
 * 					Optional, only needed if display type requires it. e.g. SSD1306 vs IL9341
 */
LineMenu::LineMenu(Adafruit_GFX *gfx, bool autoNo, void (*showDsp)(void)) : numberOfElements(0)
{
	_isDisplayed = false;
	_theDisplay = gfx;
	selectTop();
	autoNumber=autoNo;
	maxStringLength = LM_MAX_LINE_LENGTH;
	// init the menu items array. Only need to do the first element.
	menuElements[0].menuString=0;
	menuElements[0].doActivated=NULL;
	if(!showDsp)
		showDisplay = this->noUpdate;
	else
		showDisplay = showDsp;
}

/**
 * Begin the menu system.
 * Put the display in a usable mode for text.
 * @todo Add optional arguments. For now assumes B&W display with fg white, bg black. Font scale 1X.
 */
void LineMenu::begin(void)
{
	_theDisplay->cp437(true);		/// Do the right thing!
	_theDisplay->setTextColor(1,0);	/// @todo make fg,bg color settable via argument.
	_theDisplay->setTextSize(1);	/// Draw 1X-scale text. @todo make this settable via argument.

	eraseDisplay();					/// @todo Do we want to do this or not?
	_theDisplay->setCursor(0,0);
}

/**
 * Display the menu starting with the specified element number at the top of the display.
 *
 * @param topElement (default 0) element in the menu list to start with at the top of display.
 */
void LineMenu::refresh(int topElement)
{
	if(topElement < LM_FIRST_ELEMENT)
		return;
	// reset display cursor to upper left with one char space for cursor..
	_theDisplay->setCursor(0,0);
	eraseDisplay();			// todo: or erase line by line?
	for(int theLine = topElement;
			theLine < (topElement + LM_MAX_LINES)
			&& theLine < LM_TOTAL_ELEMENTS
			//&& menuElements[theLine].menuString != 0
			&& theLine < numberOfElements
			;
			theLine++)
	{
		// Mark the selected menu item.  Todo: For serial port menu, skip this entire conditional..
		if(_cursorElement == theLine && menuElements[theLine].doActivated != NULL)
		{
			_theDisplay->write(LM_CURSOR_ICON_RT);
		}else if(_cursorElement == theLine)
		{
			_theDisplay->write(LM_CURSOR_ICON_NIL);
		}else
			_theDisplay->print(' ');
		// todo: add auto number support.
		_theDisplay->print(menuElements[theLine].menuString);
		_theDisplay->write('\n');
	}
	showDisplay();
	_isDisplayed = true;
}

/**
 * Add a menu string to the menu system with a callback to be called
 * when the item is activated.
 *
 * @param menuString	name for the menu item
 * @param callback		optional pointer to function to run when menu item is activated.
 *
 * @return the index of the added item in the menu list. Else -1 if menu list full.
 */
int LineMenu::addMenuElementText(const char* menuString, void(*callback)(void))
{
	if(numberOfElements >= LM_TOTAL_ELEMENTS )
		return -1;
	int addedNo=numberOfElements;
	menuElements[numberOfElements].menuString=menuString;
	menuElements[numberOfElements].doActivated=callback;
	numberOfElements++;
	return addedNo;
}

/**
 * @return the number of menu items
 */
int LineMenu::getCount(void)
{
	return numberOfElements;
}

/**
 * Update an item in the menu at the specified location.
 * 
 * @param itemNumber to update. Range LM_FIRST_ELEMENT to LM_LAST_ELEMENT
 * @param menuString	name for the menu item
 * @param callback		optional pointer to function to run when menu item is activated.
 * @return -1 if itemNumber out of range, else itemNumber.
 */
int LineMenu::updateMenuElement(int itemNumber, const char* menuString, void(*callback)(void))
{
	if(itemNumber < LM_FIRST_ELEMENT || itemNumber > (LM_LAST_ELEMENT) )
		return -1;

	menuElements[itemNumber].menuString=menuString;
	menuElements[itemNumber].doActivated=callback;
	return itemNumber;
}

/**
 * Remove an item from the menu at the specified location.
 * @todo Refactor this. Currently it terminates the list at removed item. Should only make removed line blank ("").
 *
 * @param itemNumber to remove. Range LM_FIRST_ELEMENT to LM_LAST_ELEMENT
 * @return -1 if itemNumber out of range, else itemNumber.
 */
int LineMenu::removeMenuElement(int itemNumber)
{
	if(itemNumber < LM_FIRST_ELEMENT || itemNumber > (LM_LAST_ELEMENT) )
		return -1;

	menuElements[itemNumber].menuString=0;
	menuElements[itemNumber].doActivated=NULL;
	numberOfElements--;
	return itemNumber;
}

/**
 * hide the menu from the display.
 * Menu elements unchanged.
 * @todo For a Serial interface, this can be a NOP.
 */
void LineMenu::hide(void)
{
	eraseDisplay();
}

/**
 * Clear the menu element list
 * */
void LineMenu::clear(void)
{
	for(int i = 0; i < LM_TOTAL_ELEMENTS; i++)
	{
		menuElements[i].menuString=0;
		menuElements[i].doActivated=NULL;
	}
	numberOfElements = 0;
	selectItem(LM_FIRST_ELEMENT);
}

/**
 * Set the selected item as requested.
 * @param item	menu item to set as selected
 * @return item number if selected, -1 if out of range.
 */
int LineMenu::selectItem(int item)
{
	// range check!
	if(item < LM_FIRST_ELEMENT || item > (LM_LAST_ELEMENT) || item >= numberOfElements)
		return -1;
	_cursorElement = item;
	return item;
}

/// Set the first item in the menu list as selected.
/// @return item number if selected, -1 if out of range.
int LineMenu::selectTop(void)
{
	return selectItem(LM_FIRST_ELEMENT);
}

/// Set the next item in the menu list as selected.
/// @return item number if selected, -1 if selector was at the end of list.
int LineMenu::selectNext(void)
{
	return selectItem(getSelected()+1);
}

/// Set the previous item in the menu list as selected.
/// @return item number if selected, -1 if selector was at the top of list.
int LineMenu::selectPrevious(void)
{
	return selectItem(getSelected()-1);
}

/// Get the menu item number of the currently selected item.
/// @return selected item number or -1 if no selected item due to empty menu.
int LineMenu::getSelected(void)
{
	if(numberOfElements)
		return _cursorElement;
	return -1;
}

/// Run a menu item's action callback.
/// @param item		optional - the menu item to run the callback.
///					if no item given, then uses previously selected item.
void LineMenu::runSelected(int item)
{
	selectItem(item);
	if(_cursorElement >= 0 && menuElements[_cursorElement].doActivated)
		menuElements[_cursorElement].doActivated();
}

/// @return true if menu is showing on the display's screen. false if hidden.
bool LineMenu::isShowing(void)
{
	return _isDisplayed;
}


// --------------- The private parts --------------------------

/**
 * Fill the screen to all background color.
 */
void LineMenu::eraseDisplay(void)
{
	_theDisplay->fillScreen(0);		/// @todo make bg color settable via argument to begin.
	showDisplay();
	_isDisplayed=false;
}

/// Used for the case where the display class does not have a function to display
/// the actual data on the screen.. e.g. SSD1306 vs IL9341
/// Required as a work-around to polymorphic-limited implementation of Adafruit GFX base class. *sigh*
void LineMenu::noUpdate(void)
{
	return;
}
