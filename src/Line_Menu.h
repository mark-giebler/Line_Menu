/// @file Line_Menu.h
///
/// Line menu UI class
/// For managing Menu driven interfaces via
/// Displays or (someday) Serial ports
///
/// Mark Giebler
/// 2022-09-20

#ifndef __Line_Menu_H__
#define __Line_Menu_H__

#include <Adafruit_GFX.h>

#define LM_TOTAL_ELEMENTS		(40)
#define LM_FIRST_ELEMENT		(0)
#define LM_LAST_ELEMENT			(LM_TOTAL_ELEMENTS-1)

#define LM_MAX_LINE_LENGTH		(20)	///< characters per string for menu line. Typical for 128 pixel display and default font..
#define LM_MAX_LINES			(8)		///< menu strings per display screen. @todo make dynamic based on display size/font size.
#define LM_CHAR_WIDTH			(6)		///< pixels width of one character. Default font.

										// Cursor icons. See here: https://learn.adafruit.com/assets/103682
#define LM_CURSOR_ICON_RT		(0x10)	///< Right pointing Cursor icon character for default font.
#define LM_CURSOR_ICON_LT		(0x11)	///< Left pointing  Cursor icon character for default font.
#define LM_CURSOR_ICON_NIL		(0x16)	///< Cursor icon character for no action line. Default font.
/**
 * A structure for holding menu line elements
 **/
typedef struct elements{
	const char* menuString;		//!< Title for the menu element
	void(*doActivated)(void);	//!< optional callback to call for the menu element
} menuElements_t;


class LineMenu{
public:
	LineMenu(Adafruit_GFX *gfx, bool autoNumber = false, void (*showDisplay)(void) = NULL);
	/// @todo Add a constructor that takes a Serial interface object. Or derive a new class for that?
	void begin(void);
	int addMenuElementText(const char* menuString,  void(*callbackActivated)(void));
	int updateMenuElement(int itemNumber, const char* menuString, void(*callback)(void));
	int removeMenuElement(int itemNumber);
	void refresh(int topElement=0);
	void hide(void);
	void clear(void);
	bool isShowing(void);
	int selectTop(void);
	int selectNext(void);
	int selectPrevious(void);
	int selectItem(int item);
	void runSelected(int item = -1);
	int getSelected(void);		// -1 if nothing selected or menu list empty.
	int getCount(void);

private:
	Adafruit_GFX *_theDisplay;
	bool autoNumber;			// auto number on add element
	bool _isDisplayed;
	int _cursorElement;
	int numberOfElements;
	menuElements_t menuElements[LM_TOTAL_ELEMENTS+1]; // Todo: allocate off the heap
	int maxStringLength;
	// private helpers
	void (*showDisplay)(void);	// ptr to function used to actually show data on display. Only some display classes require it.
	void eraseDisplay(void);
	static void noUpdate(void);
};

#endif	// __Line_Menu_H__
