# A Line Menu Library for Arduino

A simple class to manage line based menus on displays.

Created by Mark Giebler.

## Dependencies

Depends on:

- Adafruit GFX Library
- GFX based Display Device library; such as SSD1306, IL9341, etc.

