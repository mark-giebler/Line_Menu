/// @file line_menu_demo.ino
///
/// Line Menu UI class
/// Example sketch
///
/// Example Uses an I2C SSD1306 type display.
///
/// Mark Giebler
/// 2022-09-20

// bit rate to run the Serial interface
#define BITRATE			230400

#include <SPI.h>
#include <Wire.h>
// We will use the Adafruit libraries.
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define BLACK SSD1306_BLACK     ///< Draw 'off' pixels
#define WHITE SSD1306_WHITE     ///< Draw 'on' pixels

#define SCREEN_WIDTH	128		// OLED display width, in pixels
#define SCREEN_HEIGHT	64		// OLED display height, in pixels
#define OLED_RESET		-1		// Reset pin # (or -1 if not used)
#define SCREEN_ADDRESS	0x3C	///< See data sheet for Address; 
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);


#include <Line_Menu.h>
LineMenu myMenu(&display, false, displayShow);

/**
 * required for SSD1306 class implementation
 * in order for changes to be updated to the display screen.
 */
void displayShow(void)
{
	display.display();
}

// declare functions to execute for each menu item.
void my_menu_func1(void)
{
	Serial.println(__func__);
}
void my_menu_func2(void)
{
	Serial.println(__func__);
}
void my_menu_func3(void)
{
	Serial.println(__func__);
}
void my_menu_func4(void)
{
	Serial.println(__func__);
	// show menu starting with line 4 (0th indexing)
	myMenu.refresh(3);
	delay(1000);
	myMenu.refresh();	// back to line 1 at top.
}

// --------------------------------------------
// Display Menu List
//
// A list with the menu item's title and it's callback function.
// The callback function is called when the menu item is activated.
// Menu title maximum length is 20 characters for a 128 pixel wide screen.
// Which leaves 1 character space for the cursor.
// The type menuElements_t is defined in the Line_Menu definition.
// --------------------------------------------
const menuElements_t ourMenuList[] = {
	"Do Something 1",		my_menu_func1,
	"Do Something 2",		my_menu_func2,
	"Do Something 3",		my_menu_func3,
	"Line 4", 				my_menu_func4,
	"Line 5", 				NULL,
	"Line 6 ___1234567890", 	NULL,

	0,NULL	// must be last.
};

void setupMenuList(void)
{
	int i=0;
	while(ourMenuList[i].menuString)
	{
		if(myMenu.addMenuElementText(ourMenuList[i].menuString, ourMenuList[i].doActivated) == -1)
			break;
		i++;
	}
}

void setup()
{
	Serial.begin(BITRATE);
	Serial.println(" Mark Giebler.   2022-09-20");
	Serial.println("Line Menu class example");
	display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS);

	myMenu.begin();
	// load up our menu list into the display's buffer
	setupMenuList();
	// Add a few more menu lines. 8 lines fit on a 64 row display.
	myMenu.addMenuElementText("Line 7", NULL);
	myMenu.addMenuElementText("Line 8", NULL);
	// display the menu list with item 1 at the top.
	myMenu.refresh();
}

void loop()
{
	delay(1500);
	// hide the menu
	myMenu.hide();
	delay(800);

	// simulate selecting menu items and executing the item's action function.
	line_menu_selection_test();
}


void line_menu_selection_test(void)
{
	delay(500);
	Serial.println(__LINE__);
	myMenu.selectTop();		// line 1
	Serial.print("Select: "); Serial.println(myMenu.getSelected());
	myMenu.refresh();delay(500);
	myMenu.runSelected();
	delay(500);
	myMenu.selectNext();	// line 2
	Serial.print("Select: "); Serial.println(myMenu.getSelected());
	myMenu.refresh();delay(500);
	myMenu.runSelected();
	delay(500);
	myMenu.selectNext();	// line 3
	Serial.print("Select: "); Serial.println(myMenu.getSelected());
	myMenu.refresh();delay(500);
	myMenu.runSelected();
	delay(500);
	myMenu.selectNext();	// line 4
	Serial.print("Select: "); Serial.println(myMenu.getSelected());
	myMenu.refresh();delay(500);
	myMenu.runSelected();
	delay(500);
	myMenu.selectPrevious();// line 3
	Serial.print("Select: "); Serial.println(myMenu.getSelected());
	myMenu.refresh();delay(500);
	myMenu.selectPrevious();// line 2
	Serial.print("Select: "); Serial.println(myMenu.getSelected());
	myMenu.refresh();delay(500);
	myMenu.selectTop();		// line 1
	Serial.print("Select: "); Serial.println(myMenu.getSelected());
	myMenu.refresh();
	Serial.println(__LINE__);
}
